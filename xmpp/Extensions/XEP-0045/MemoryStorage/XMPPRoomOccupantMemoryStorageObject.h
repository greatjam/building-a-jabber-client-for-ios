#import <Foundation/Foundation.h>
#import "XMPPRoom.h"
#import "XMPPRoomOccupant.h"


@interface XMPPRoomOccupantMemoryStorageObject : NSObject <XMPPRoomOccupant, NSCopying, NSCoding>

- (id)initWithPresence:(XMPPPresence *)presence;
- (void)updateWithPresence:(XMPPPresence *)presence;

/**
 * The properties below are documented in the XMPPRoomOccupant protocol.
**/

@property (weak, readonly) XMPPPresence *presence;

@property (weak, readonly) XMPPJID  * jid;
@property (weak, readonly) XMPPJID  * roomJID;
@property (weak, readonly) NSString * nickname;

@property (weak, readonly) NSString * role;
@property (weak, readonly) NSString * affiliation;
@property (weak, readonly) XMPPJID  * realJID;

/**
 * Compares two occupants based on the nickname.
 * 
 * This method provides the ordering used by XMPPRoomMemoryStorage.
 * Subclasses may override this method to provide an alternative sorting mechanism.
**/
- (NSComparisonResult)compare:(XMPPRoomOccupantMemoryStorageObject *)another;

@end
