#import <Foundation/Foundation.h>
#import "XMPPRoom.h"
#import "XMPPRoomOccupant.h"


@interface XMPPRoomOccupantHybridMemoryStorageObject : NSObject <XMPPRoomOccupant, NSCopying, NSCoding>

- (id)initWithPresence:(XMPPPresence *)presence streamFullJid:(XMPPJID *)streamFullJid;
- (void)updateWithPresence:(XMPPPresence *)presence;

/**
 * The properties below are documented in the XMPPRoomOccupant protocol.
**/

@property (weak, readonly) XMPPPresence *presence;

@property (weak, readonly) XMPPJID  * jid;
@property (weak, readonly) XMPPJID  * roomJID;
@property (weak, readonly) NSString * nickname;

@property (weak, readonly) NSString * role;
@property (weak, readonly) NSString * affiliation;
@property (weak, readonly) XMPPJID  * realJID;

@property (weak, readonly) NSDate * createdAt;

/**
 * Since XMPPRoomHybridStorage supports multiple xmppStreams,
 * this property may be used to differentiate between occupant objects.
**/
@property (weak, readonly) XMPPJID  * streamFullJid;

/**
 * Sample comparison methods.
**/

- (NSComparisonResult)compareByNickname:(XMPPRoomOccupantHybridMemoryStorageObject *)another;

- (NSComparisonResult)compareByCreatedAt:(XMPPRoomOccupantHybridMemoryStorageObject *)another;

@end
